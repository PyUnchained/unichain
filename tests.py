import random
import string
import time
from django.test import TestCase

from unichain.core.tools import MultiChainHandler

# Create your tests here.
class StartUpTestCase(TestCase):

	def test_core(self):

		#Setup everything
		test_asset_name = ''.join(random.SystemRandom().choice(string.ascii_uppercase + string.digits) for _ in range(10))
		handler = MultiChainHandler('d')
		self.assertTrue(handler.instruct('new_chain').is_okay)

		#Create testing addresses
		resp = handler.instruct('getnewaddress')
		test_address_first = resp.info['new_address']
		resp = handler.instruct('getnewaddress')
		test_address_second = resp.info['new_address']

		#Issue initial currency to first account...
		resp_obj = handler.instruct('issue', address = test_address_first,
			quantity = 2000, asset = test_asset_name)

		#Move some money around
		resp = handler.instruct('sendassetfrom', from_address = test_address_first,
			to_address = test_address_second, quantity = 100, asset = test_asset_name)
		resp = handler.instruct('sendassetfrom', from_address = test_address_second,
			to_address = test_address_first, quantity = 50, asset = test_asset_name)
		resp = handler.instruct('sendassetfrom', from_address = test_address_first,
			to_address = test_address_second, quantity = 2.50, asset = test_asset_name)
		resp = handler.instruct('sendassetfrom', from_address = test_address_second,
			to_address = test_address_first, quantity = 2.50, asset = test_asset_name)

		#Check that the two accounts have the balance we expect them to have
		self.assertEqual(handler.getaddressbalances(address = test_address_first).body['qty'], 1950)
		self.assertEqual(handler.getaddressbalances(address = test_address_second).body['qty'], 50)
		
		
