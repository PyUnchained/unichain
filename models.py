from __future__ import unicode_literals

from django.db import models
from picklefield.fields import PickledObjectField

# Create your models here.
class MinerSignal(models.Model):
	method = models.CharField(max_length = 100)
	params = models.CharField(max_length = 100)
	sid = models.PositiveIntegerField()
	chain_name = models.CharField(max_length = 100)
	date_time = models.DateTimeField(auto_now_add=True)
	async_done = models.BooleanField(default = False)
	evaluator_func = PickledObjectField(null = True, blank = True)
	evaluator = PickledObjectField(null = True, blank = True)


	@classmethod
	def create(cls, head_dict):

		obj = cls(method = head_dict['method'], params = head_dict['params'],
			sid = head_dict['id'], chain_name = head_dict['chain_name'],
			date_time = timezone.now())
		return obj

