##Errors thrown by Blocknet clients
class BlocknetFailedExecutionError(RuntimeError):
    '''raised when the program fails to execute a multichain command'''

class BlocknetArgumentError(RuntimeError):
    '''raised when incorrect arguments are passed to the function'''

class BlocknetMultiChainError(RuntimeError):
    '''raised when MultiChain returns an error message for a command'''

class UnichainCommandError(RuntimeError):
    '''raised when MultiChain returns an error message for a command'''