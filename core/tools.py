from pexpect import run, spawn
from Queue import Queue, Empty
from shlex import split
from subprocess import PIPE, Popen
from threading  import Thread
from threading  import Thread
import ast, time
import json
import re
import requests
import select
import socket
import subprocess
import sys
import time

from django.conf import settings
from django.utils import timezone 

from errors import*


class MultichainResponse():
    """
    Object representing the response of executing a command on the blockchain.
    [self.success] - Boolean value showing whether the intented command was carried out or not.
    [self.response] - The text response returned when the command was executed.
    """

    def __init__(self, success, response, extra = {}, *args, **kwargs):
        self.success = success
        self.response = response
        self.head_match = None
        self.body_match = None
        self.info = extra
        self.generate_info()  #Generates info dict

    def generate_info(self):
        """
        Collates and organizes all of the important information returned by the call to
        MultiChain.
        """
        self.info.update({'okay':self.success, 'resp_txt':self.response, 'time_stamp':timezone.now()})

        if isinstance(self.response, basestring):
            head_pattern = re.compile(r'({\S*})')
            head_match = head_pattern.match(self.response)
            if head_match:
                self.head_match = head_match
                head_str = head_match.group(0).strip()
                head_str =head_str.replace('true', 'True')
                head_str =head_str.replace('false', 'False')
                self.head = ast.literal_eval(head_str)

            #Find Transaction id...
            regex_lookup = re.compile(r"\n\w{10,90}") #Regex to find new address
            regex_match = re.search(regex_lookup,self.response)
            if regex_match:
                self.info.update({'txid':regex_match.group(0).strip('\n')})

            if head_match:
                body_index = head_match.end()
            else:
                body_index = 0

            try:
                self.body = json.loads(self.response[body_index:].strip())
                if type(self.body) == list:
                    if len(self.body) == 1:
                        self.body = self.body[0]
                        self.info.update(self.body)
                    elif len(self.body) > 1:
                        self.info.update({'items':self.body})
            
            except ValueError:
                body_txt = self.response[body_index:].replace('\n','')  #Remove all new lines to make regex search simpler
                body_txt = body_txt.replace('false','False')
                body_txt = body_txt.replace('true','True')  #Convert JSON variables to python values for ast.literal_eval
                body_pattern = re.compile(r'({.*})')
                body_match = body_pattern.match(body_txt)
                if body_match:
                    self.body_match = body_match
                    self.body = ast.literal_eval(body_match.group(0).strip())
                    self.info.update(self.body)

    @property
    def is_okay(self):
        """Boolean showing whether or not the response succeeded."""
        return self.info['okay']

    def __str__(self):
        return str(self.info)

class CommandLineInterface():
    """
    Mixin to allow an object to send command-line instruction (base class for MultiChainHandler)
    """

    def run(self, cmd):
        """
        Executes the given command and detaches the terminal process from the main thread,
        but retaining the PID of the new process.
        """
        p = subprocess.Popen(split(cmd), stdout=subprocess.PIPE)
        return p.pid

    def run_communicate(self, cmd):
        """
        Executes the given command keeping the terminal process in the main thread and returning
        the response from the command line.
        """

        #If there's a pipe character in the command, it must be openned with the shell interpreter to work properly
        if '|' in cmd:
            p = subprocess.Popen(cmd, stdout=subprocess.PIPE, shell = True)
        else:
            p = subprocess.Popen(split(cmd), stdout=subprocess.PIPE)

        output, err = p.communicate()
        return output


class MultiChainHandler(CommandLineInterface):
    """
    Handles communication between the Django app and MultiChain.
    """
    
    def __init__(self, chain):
        self.POSITIVE_RESPS = {
        'clone':['Network parameter set was successfully cloned', 'already exists',
            'parameter set was successfully cloned'],
        'initialize':['New users can connect to this blockchain using',
            'Probably multichaind for this network is already running','multichaind'],
        'general':['error']}
        self.RESTART_RESPS = {
        'chain':["error: couldn't connect to server"]
        }
        self.chain = chain
        self.running_at_init = False
        if self.get_chain_pid() != None:
            self.running_at_init = True



    def instruct(self, command, **kwargs):
        """
        Issues an instruction to MultiChain.
        """
        self.command = command
        if self.running_at_init == False:
            self.initialize()


        if command not in ['new_chain']:
            if not self.running_at_init:
                try:
                    self.initialize()
                except BlocknetFailedExecutionError:
                    pass

        try:
            func = getattr(self, command)
            return func(**kwargs)
        except AttributeError:
            raise UnichainCommandError('The following command is not valid: %s' % comand)

    def getaddressbalances(self, address = None):
        """
        Retrieves the asset balances for a specific address on the blockchain.
        """
        cmd = "multichain-cli {0} getaddressbalances {1}".format(self.chain, address)
        resp = self.run_communicate(cmd)
        return MultichainResponse(True, resp)

    def get_issuing_address(self):
        """
        Retrieve the address that has the rights to issue new assets.
        """
        cmd = 'multichain-cli %s listpermissions issue' % self.chain
        resp = self.run_communicate(cmd)
        regex_lookup = re.compile(r"1\w{10,90}") #Regex to find new address
        regex_match = re.search(regex_lookup,resp)
        if regex_match:
            extra = {'issuing_address':regex_match.group(0)}
            return MultichainResponse(True, resp, extra = extra)

        return MultichainResponse(False, resp)


    def get_chain_pid(self):
        """Returns the PID of the currently running chain or None if it is not
        currently running."""
        cmd = 'ps ax | grep multichain'
        resp = self.run_communicate(cmd)    
        running_processes = resp.split('\n')
        for rp in running_processes:
            if self.chain in rp:
                index = rp.find('?')
                pid = rp[:index]
                return int(pid.strip())

        return None


    def issue(self, address = None, asset = 'USD', quantity = None,
        units = 0.01, **kwargs):
        """
        Initial issue of an asset into a specific account.
        """

        if address == None or quantity == None:
            raise BlocknetArgumentError("Keyword arguments 'address' and 'quantity' are required." )

        cmd_start = 'multichain-cli {0} issue {1}'.format(self.chain, address)
        cmd_end = "'{0}' {1} {2}".format(json.dumps({"name":asset,"open":True}), quantity, units)
        cmd = ' '.join([cmd_start, cmd_end])
        resp = self.run_communicate(cmd)

        regex_lookup = re.compile(r"\n\w{10,90}")
        regex_match = re.search(regex_lookup,resp)
        if regex_match:
            time.sleep(15)
            return MultichainResponse(True,resp)
        else:
            return MultichainResponse(False, resp)
        


    def new_chain(self, **kwargs):
        """
        Creates and initializes a new blockchain.
        """

        cmd = 'multichain-util create %s' % self.chain
        resp_txt = self.run_communicate(cmd)

        #If the new blockchain was created or already existed....
        if 'Blockchain parameter set was successfully generated.' in resp_txt:
            try:                                        #initialize it...
                pid = self.initialize()
                resp = MultichainResponse(True, resp_txt, exta = {'pid':pid})
            except BlocknetFailedExecutionError as e:   #Handle any errors thrown whilst initializing...
                resp = MultichainResponse(False, str(e))

        #If the new blockchain already exists....
        if 'file /root/.multichain/{0}/params.dat already exists'.format(self.chain) in resp_txt:
            pid = self.get_chain_pid()
            if pid == None:
                pid = self.initialize()
            resp = MultichainResponse(True, resp_txt, exta = {'pid':pid})

        #If the blockchain wasn't created, return the error message thrown
        else:
            success = False
            resp = MultichainResponse(success, resp_txt)

        return resp

    def getinfo(self, restart = True, **kwargs):
        """
        Returns information about a particular chain.
        """
        cmd = 'multichain-cli %s getinfo' % self.chain
        resp = self.run_communicate(cmd)

        #If there's no response, assume that the chain hasn't been initialized
        #and reinitialize it.
        
        if (resp == "error: couldn't connect to server") or (resp == '') :

            #Try restarting the server....
            if restart:
                self.initialize()
                time.sleep(1)         #Pause to allow chain to start
                resp = self.getinfo(restart = False)

            #If the chain has been restarted but still can't connect to server...
            else:
                success = False
                resp = 'Chain with this name does not exist, or has not yet been properly configured.'

        else:
            success = True

        #If the chain was restarted, the resp will be a MultichainResponse object, not a string...
        if isinstance(resp, MultichainResponse):
            return resp
        else:
            return MultichainResponse(success, resp)


    def getnewaddress(self, retry = True):
        """
        Creates a new address on the blockchain.
        """
        cmd = 'multichain-cli %s getnewaddress' % self.chain
        resp = self.run_communicate(cmd)
        regex_lookup = re.compile(r"1\w{10,90}") #Regex to find new address
        regex_match = re.search(regex_lookup,resp)
        if regex_match:
            extra = {'new_address':regex_match.group(0)}
            cmd = 'multichain-cli {0} grant {1} send,receive'.format(self.chain, regex_match.group(0))
            self.run(cmd)
            return MultichainResponse(True, resp, extra = extra) 
        else:
            return MultichainResponse(False, resp) 

    def initialize(self):
        """
        Starts the process of mining the blockchain.
        """

        cmd = 'multichaind %s -daemon -miningrequirespeers=0' % self.chain
        pid = self.run(cmd)
        new_cmd = 'ps -p {0} -o comm='.format(pid) #Make sure process is running
        resp = self.run_communicate(new_cmd)
        if self.resp_ok('initialize', resp):
            return pid
        if resp == '':
            resp = 'Either chain does not exist or has not been properly configured.'
        raise BlocknetFailedExecutionError(resp)  

    def resp_ok(self, response_class, resp):
        """
        Determines if the response returned indicates a successful transaction.
        """
        for i in self.POSITIVE_RESPS[response_class]:
            if i in resp:
                return True
        return False

    def sendassetfrom(self, from_address = None, to_address = None, asset = 'USD', quantity = None):
        """
        Sends a certain amount of an asset from one defined account to another
        one. Optionally, one can specify the type of asset to send.
        """

        #Determine if the optional asset argument is present
        cmd = 'multichain-cli {0} sendassetfrom {1} {2} {3} {4}'.format(
            self.chain, from_address, to_address, asset, quantity)
        resp = self.run_communicate(cmd)

        regex_lookup = re.compile(r"\n\w{10,90}")
        regex_match = re.search(regex_lookup,resp)
        if regex_match:
            return MultichainResponse(True,resp)
        return MultichainResponse(False, resp)